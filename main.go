package main

import (
	"fmt"
	"genCodeRandom/utils"
	"log"
	"strings"
	"sync"
	"time"
)

type Job struct {
	number int
}

var jobs = make(chan Job, 100)
var results = make(chan string, 100)
var resultCollection []string

func main()  {
	now := time.Now()
	// allocate jobs
	noOfJobs := 3000
	go allocateJobs(noOfJobs)

	// get results
	done := make(chan bool)
	go getResults(done)

	// create worker pool
	noOfWorkers := 100
	createWorkerPool(noOfWorkers)

	// wait for all results to be collected
	<-done

	data := strings.Join(resultCollection, "\n")

	// write json data to file
	err := utils.WriteToFile([]byte(data), "code")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("SUCCESS")
	log.Println("time finish", time.Since(now))
}

func allocateJobs(noOfJobs int) {
	for i := 0; i <= noOfJobs; i++ {
		jobs <- Job{i + 1}
	}
	close(jobs)
}

func worker(wg *sync.WaitGroup) {
	for job := range jobs {
		result := utils.GenRandomString(job.number)
		results <- result
	}
	wg.Done()
}

func getResults(done chan bool) {
	for result := range results {
		fmt.Printf("Retrieving issue #%d\n", result)
		resultCollection = append(resultCollection, result)
	}
	done <- true
}


func createWorkerPool(noOfWorkers int) {
	var wg sync.WaitGroup
	for i := 0; i <= noOfWorkers; i++ {
		wg.Add(1)
		go worker(&wg)
	}
	wg.Wait()
	defer close(results)
}